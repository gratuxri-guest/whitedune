#! /usr/bin/make -f
# -*- makefile -*-
package=whitedune
#export DH_VERBOSE=1

DEB_HOST_GNU_TYPE   ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)

DESTDIR?=$(CURDIR)/debian/${package}/usr

config.status: configure
	dh_testdir
	./configure \
		$(shell env DEB_LDFLAGS_MAINT_APPEND="-Wl,-z,defs -Wl,--as-needed" dpkg-buildflags --export=cmdline) \
		--host=$(DEB_HOST_GNU_TYPE) \
		--build=$(DEB_BUILD_GNU_TYPE) \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--without-usrlocalinclude \
		--without-usrlocallib

configure:
	-${MAKE} bootstrap
	autoconf
ifneq "$(wildcard /usr/share/misc/config.sub)" ""
	cp -f /usr/share/misc/config.sub config.sub
endif
ifneq "$(wildcard /usr/share/misc/config.guess)" ""
	cp -f /usr/share/misc/config.guess config.guess
endif

build: build-arch build-indep
build-arch: build-stamp

build-indep:

build-stamp:  config.status
	dh_testdir
	$(MAKE)
#	cp man/dune.1 debian/
	sed -e 's/dune/${package}/g' < ./man/dune.1 \
	 | sed -e 's/DUNE/WHITEDUNE/g' > debian/${package}.1
	cd debian && ln -fs ${package}.1 dune.1
	touch $@

generated_files?=\
 configure \
 docs/export_example_c++/Makefile \
 docs/export_example_c/Makefile \
 docs/export_example_java/Makefile \
 src/Makefile \
 src/SDLjoystick/Makefile \
 src/swt/motif/Makefile \
 src/swt/rc/Makefile \
 src/swt/tests/Makefile \
 test/Makefile \
 #}

clean:
	dh_testdir
	dh_testroot
	rm -f build-stamp
	$(MAKE) clean realclean distclean  || echo "# $@"
	rm -f ${generated_files}
	dh_clean

install: build-arch
	dh_testdir -a
	dh_testroot -a
	dh_prep -a
	dh_installdirs -a
	install -d ${DESTDIR}/bin
	install bin/dune ${DESTDIR}/bin/${package}
	install -d ${DESTDIR}/share/pixmaps/
	install -m 644 ./desktop/fvwm2/mini-dune.xpm \
	  ${DESTDIR}/share/pixmaps/${package}.xpm

# find docs -type f > debian/${package}-doc.docs
binary-indep:
	dh_testdir -i
	dh_testroot -i
	dh_install -i
	dh_installdocs -i
	dh_installexamples -i
	dh_installchangelogs -i
	dh_link -i
	dh_compress -i
	dh_fixperms -i
	dh_installdeb -i
	dh_gencontrol -i
	dh_md5sums -i
	dh_builddeb -i

binary-arch: install
	dh_testdir -a
	dh_testroot -a
	dh_installchangelogs -a
	dh_installdocs -a
	dh_installexamples -a
	dh_install -a
	dh_installmenu -a
	dh_installmime -a
	dh_installman -a
	dh_link -a
	dh_strip -a
	dh_compress -a
	dh_fixperms -a
	dh_shlibdeps -a
	dh_installdeb -a
	dh_gencontrol -a
	dh_md5sums -a
	dh_builddeb -a

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install

#eof "$Id: rules -- rzr $"
